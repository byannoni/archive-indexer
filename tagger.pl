#!/usr/bin/env perl

# Copyright (c) 2019-2020 Brandon Yannoni
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

use strict;
use warnings;
use 5.010_000; ## Perl 5.10 required for usage of state
use feature 'state';
use English '-no_match_vars';
use File::Basename;
use File::Copy 'cp';
use File::Glob ':bsd_glob';
use Getopt::Long qw(:config posix_default bundling_override auto_version);
use POSIX 'strftime';
use Pod::Usage;
use Time::HiRes 'time';

our $VERSION = '1.5.4';
my $log_level = 2;

use constant {
	FATAL => 0,
	ERROR => 1,
	INFO => 2,
	DEBUG => 3
};

exit main();

sub main
{
	my %tags;
	my %tags_to_names;
	my %files_to_desc;
	my $tag_list_filename;
	my $index_filename;
	my $toc_filename;
	my $interactive;
	my $backup = 1;
	my @files_to_index;
	my $toc_head = ',,,,,';
	my %args = (
		contents => \$toc_filename,
		debug => sub { $log_level = 3 },
		h => sub { pod2usage( -verbose => 1, -exitval => 0) },
		help => sub { pod2usage( -verbose => 2, -exitval => 0) },
		index => \$index_filename,
		quiet => sub { $log_level = 1 },
		'tag-list' => \$tag_list_filename,
		usage => sub { pod2usage( -verbose => 0, -exitval => 0) },
		verbose => \$log_level,
		backup => \$backup,
	);

	pod2usage('Error while processing options. Specify --help for help.')
			unless GetOptions(\%args, 'debug|d', 'quiet|q', 'h|?',
			'verbose:3', 'tag-list|t=s', 'index|i=s', 'help',
			'contents|c=s', 'usage', 'backup!',
			'' => \$interactive);

	if ($log_level >= 3) {
		select STDERR;
		++$OUTPUT_AUTOFLUSH;
		select STDOUT;
		++$OUTPUT_AUTOFLUSH;
	}

	logmsg(DEBUG, "Version $VERSION; Perl $PERL_VERSION");
	logmsg(FATAL, 'Option --tag-list is required')
			unless defined ${$args{'tag-list'}};
	logmsg(INFO, 'Neither --index nor --contents was specified. Was this intended?')
			unless defined ${$args{index}}
			or defined ${$args{contents}};
	logmsg(FATAL, 'Failed to read list of tags')
			unless get_tags($tag_list_filename, \%tags,
			\%tags_to_names);
	logmsg(FATAL, 'No tags found') unless keys %tags_to_names;

	if ($OSNAME =~ m'^MSWin') {
		# Fix Windows shell globbing
		push @files_to_index, map glob, @ARGV;
	} else {
		push @files_to_index, @ARGV;
	}

	push @files_to_index, map glob, <> if $interactive;
	logmsg(DEBUG, "Found file to index: '$_'") foreach @files_to_index;

	if ($toc_filename and -e $toc_filename) {
		logmsg(FATAL, "Failed to backup TOC file '$toc_filename'")
				unless not $backup
				or cp $toc_filename, "$toc_filename.bak";
		logmsg(ERROR, "Failed to read TOC file '$toc_filename'")
				unless read_toc($toc_filename, \%tags,
				\%files_to_desc, \$toc_head);
	}

	foreach my $filename (@files_to_index) {
		logmsg(ERROR, "Failed to index file: '$filename'")
				unless index_file($filename, \%tags,
				\%tags_to_names);
	}

	normalize_identifiers(\%tags);
	logmsg(ERROR, 'Failed to write index file')
			unless not defined $index_filename
			or write_index($index_filename, \%tags);
	logmsg(ERROR, 'Failed to write TOC file')
			unless not defined $toc_filename
			or write_toc($toc_filename, \%tags, \%files_to_desc,
			$toc_head);
	logmsg(INFO, 'Done');
	return 0;
}

sub write_toc
{
	my ($filename, $tags, $files_to_desc, $orig_head) = @_;
	my %toc;
	my $file;
	local $LIST_SEPARATOR = ';';
	local $OUTPUT_FIELD_SEPARATOR = ',';
	local $OUTPUT_RECORD_SEPARATOR = "\n";

	logmsg(DEBUG, "Generating TOC");

	foreach my $tag (keys %$tags) {
		if ($tag ne '') {
			foreach my $ident (@{$$tags{$tag}}) {
				push @{$toc{$ident}}, $tag;
			}
		} else {
			foreach my $ident (@{$$tags{$tag}}) {
				$toc{$ident} = [] unless $toc{$ident};
			}
		}
	}

	logmsg(INFO, "Writing TOC to file '$filename'");

	unless (open $file, '>', $filename) {
		logmsg(ERROR, "Failed to open file $filename: $OS_ERROR");
		return 0;
	}

	print $file $orig_head;
	print $file '', 'Date', 'Description', 'Source', 'Document #',
			'Tags', 'Link';

	foreach my $source (keys %toc) {
		my @entry = parse_ident_toc($source);

		if (@entry) {
			$entry[2] = $$files_to_desc{lc $source} // '';
			logmsg(DEBUG, "Writing TOC for source '$source'");
			print $file @entry, "@{$toc{$source}}",
					qq'=HYPERLINK("$source.pdf")';
		}
	}

	close $file;
	return 1;
}

sub read_toc
{
	my ($filename, $tags, $files_to_desc, $toc_head) = @_;
	my $file;
	my $trash;

	unless (open $file, '<', $filename) {
		logmsg(ERROR, "Failed to open file $filename: $OS_ERROR");
		return 0;
	}

	logmsg(INFO, "Reading TOC from file '$filename'");
	chomp($$toc_head = <$file> // $$toc_head);
	logmsg(DEBUG, "Read TOC header '$$toc_head'");
	chomp($trash = <$file> // '');
	logmsg(DEBUG, 'Discarding TOC line 2: ' . $trash . q"'") if $trash;

	while (my $line = <$file>) {
		chomp $line;

		my ($ident, $desc, $tag_names) = parse_toc_line($line);

		unless ($ident) {
			my $num = $file->input_line_number;

			logmsg(DEBUG, "Undefined identifier at $filename:$num");
			next;
		}

		logmsg(DEBUG, "Adding identifier '$ident' with description '$desc'");
		$$files_to_desc{lc $ident} = $desc if $ident;
		update_tags_from_toc($tags, $tag_names, $ident);
	}

	close $file;
	return 1;
}

sub update_tags_from_toc
{
	my ($tags, $tag_names, $ident) = @_;

	foreach my $tag (('', split ';', $tag_names)) {
		unless ($tag eq '' or $$tags{$tag}) {
			logmsg(DEBUG, "Skipping tag '$tag' because it is not in the tags list");
			next;
		}

		logmsg(DEBUG, "Adding identifier '$ident' to tag '$tag'");
		push @{$$tags{$tag}}, $ident unless grep $_ eq $ident,
				@{$$tags{$tag}};
	}
}

sub parse_toc_line
{
	state $ymd = qr'\d{2}(?<y>\d{2})-(?<m>\d{2})-(?<d>\d{2})';
	state $mdy = qr'(?<m>\d{1,2})/(?<d>\d{1,2})/(?:\d{2})?(?<y>\d{2})';

	my ($line) = @_;

	logmsg(DEBUG, "Parsing TOC line '$line'");

	if ($line =~ m/^[^,]*,\s*(?:$ymd|$mdy)\s*,/)
	{
		my $desc_len;
		my $description;
		my $offset = $LAST_MATCH_END[0];
		my $ident = sprintf "%02u %u %u", $LAST_PAREN_MATCH{y},
				$LAST_PAREN_MATCH{m}, $LAST_PAREN_MATCH{d};

		$desc_len = get_description_len( substr $line, $offset );
		$description = substr $line, $offset, $desc_len;
		$offset += $desc_len;

		if (substr($line, $offset) =~ /^,\s*([^,]+?)\s*,\s*([^,]+?)\s*(?:,(?<t>[^,]*))?/)
		{
			$ident .= " $1 $2";
			return $ident, $description, $LAST_PAREN_MATCH{t} // '';
		}
	}

	logmsg(ERROR, "Invalid TOC line '$line'");
	return @{[]};
}

sub get_description_len
{
	my ($remaining_line) = @_;

	return index $remaining_line, ','
			if '"' ne substr $remaining_line, 0, 1;
	logmsg(DEBUG, 'TOC line has complex description');
	return length $1 if $remaining_line =~ m'("(?:[^"]*(?:"")*)*"),';
	return 0;
}

sub parse_ident_toc
{
	my ($ident) = @_;

	logmsg(DEBUG, "Parsing identifier '$ident' for TOC");
	return '', sprintf('%02u/%u/%u', $2, $3, $1), '', $4, $5
			if $ident =~ m'^(\d{2})\s+(\d{1,2})\s+(\d{1,2})\s+(\w+)\s+(\w+)';
	logmsg(ERROR, "Invalid TOC indentifier: '$ident'");
	return @{[]};
}

sub parse_filename_index
{
	my ($filename) = @_;

	$filename = basename $filename;
	logmsg(DEBUG, "Parsing filename '$filename' for index");
	return $filename if $filename =~ s'\.[^.]+$'';
}

sub write_index
{
	my ($filename, $tags) = @_;
	my $file;
	local $LIST_SEPARATOR = ' | ';
	local $OUTPUT_RECORD_SEPARATOR = "\n";

	logmsg(INFO, "Writing index to file '$filename'");

	unless (open $file, '>', $filename) {
		logmsg(ERROR, "Failed to open file $filename: $OS_ERROR");
		return 0;
	}

	foreach my $tag (sort { lc $a cmp lc $b } keys %$tags) {
		unless ($tag ne '' and @{$$tags{$tag}}) {
			logmsg(DEBUG, "Skipping index for tag '$tag'");
			next;
		}

		logmsg(DEBUG, "Writing index for tag '$tag'");
		print $file "$tag\t@{$$tags{$tag}}";
	}

	close $file;
	return 1;
}

sub index_file
{
	my ($filename, $tags, $tags_to_names) = @_;
	my $file;
	my $input;
	my $found_tags;
	my $ident = parse_filename_index($filename);

	logmsg(INFO, "Indexing file '$filename'");

	unless (open $file, '<', $filename) {
		logmsg(ERROR, "Failed to open file $filename: $OS_ERROR");
		return 0;
	}

	$input = normalize_input(<$file>);
	logmsg(DEBUG, "Searching for tags in file '$filename'");
	$found_tags = find_tags_in_text($input, $tags, $tags_to_names);
	logmsg(DEBUG, 'Found ' . scalar @$found_tags
			. " tags in file '$filename'");

	foreach my $tag ('', @$found_tags) {
		push @{$$tags{$tag}}, $ident unless grep lc $_ eq lc $ident,
				@{$$tags{$tag}};
	}

	close $file;
	return 1;
}

sub find_tags_in_text
{
	my ($text, $tags, $tags_to_names) = @_;
	my @found_tags;
	my @complex_tags = grep m' ', keys %$tags_to_names;

	while (length $text) {
		my $tag;

		# Try the fast way: single word tag -- direct lookup
		my ($word, $new_text) = split m/(?=[^-'.]*)\W/, $text, 2;

		if ($word) {
			$word = lc $word;
			logmsg(DEBUG, "Checking if '$word' is a tag");

			if (exists $$tags_to_names{$word}) {
				$tag = $$tags_to_names{$word};
				logmsg(DEBUG, "Found tags '@$tag' ($word)");
				$text = $new_text;
				push @found_tags, @$tag;
				next;
			}

			logmsg(DEBUG, "No tag '$word' found");
		}

		# Now the slow way: match multi-word tags
		foreach my $complex_tag (@complex_tags) {
			logmsg(DEBUG, "Checking if '$complex_tag' matches");
			next unless $text =~ s/^$complex_tag//;
			$tag = $$tags_to_names{$complex_tag};
			logmsg(DEBUG, "Found tags '@$tag' ($complex_tag)");
			push @found_tags, @$tag;
			last;
		}

		$text = $new_text unless $tag;
	}

	return \@found_tags;
}

sub normalize_input
{
	state $regex = qr'\s{2,}';

	my @lines = @_;
	my $full_text;

	chomp @lines;
	# Make a single lowercase line
	$full_text = join ' ', @lines;
	# Normalize spacing
	$full_text =~ s/$regex/ /g;
	return $full_text;
}

sub get_tags
{
	my ($filename, $tags, $tags_to_names) = @_;
	my $tags_input;

	logmsg(INFO, "Reading tags from file '$filename'");

	unless (open $tags_input, '<', $filename) {
		logmsg(ERROR, "Failed to open file $filename: $OS_ERROR");
		return 0;
	}

	while (my $line = <$tags_input>) {
		chomp $line;

		my ($name, $alt_tags) = parse_tags_line($line);

		if (not $name or not $alt_tags or @$alt_tags <= 0) {
			my $num = $tags_input->input_line_number;

			logmsg(DEBUG, "Invalid tags line $filename:$num");
			next;
		}
		$$tags{$name} = [];

		foreach my $alt_tag (@$alt_tags)
		{
			push @{$$tags_to_names{$alt_tag}}, $name;
			logmsg(DEBUG, "Adding tag '$alt_tag' as alternative to '$name'");
		}
	}

	close $tags_input;
	return 1;
}

sub parse_tags_line
{
	state $regex = qr/(?=[^-'.]*)\W/;

	my ($line) = @_;
	my ($name, $alternatives) = split "\t", $line;
	my @alt_tags;

	logmsg(DEBUG, "Parsing tags line '$line'");

	if ($name) {
		logmsg(DEBUG, "Found tag name '$name'");
	} else {
		logmsg(DEBUG, "Found no tag name");
		return;
	}

	if ($alternatives) {
		logmsg(DEBUG, "Parsing tags for '$name': '$alternatives'");
	} else {
		logmsg(DEBUG, "Found no tag alternatives");
		return;
	}

	@alt_tags = split '\|', $alternatives;

	# Normalize spacing
	@alt_tags = map {
		my $tag = $_;

		$tag =~ s/$regex/ /g;

		if ($tag !~ m' ') {
			(lc $tag, map lc $tag . $_, qw(. s s. 's 's.));
		} else {
			qr/(?=[^-'.]*)\W*\Q$tag\E(?:'?s)?\.?\b/i;
		}
	} @alt_tags;

	logmsg(DEBUG, "Found alternative to '$name': '$_'")
			foreach @alt_tags;
	return $name => \@alt_tags;
}

sub logmsg
{
	use constant LOG_TAGS => qw(FATAL ERROR INFO DEBUG);
	use constant LOG_FORMAT => '[%-5s] [%s.%03d] %s';
	use constant TIME_FORMAT => '%H:%M:%S';

	my ($tag, $msg) = @_;

	if ($log_level >= $tag) {
		local $OUTPUT_RECORD_SEPARATOR = "\n";

		my $t = time;
		my $time = strftime TIME_FORMAT, localtime $t;
		my $msec = ($t - int($t)) * 1000;
		my $log_line = sprintf LOG_FORMAT, (LOG_TAGS)[$tag], $time,
				$msec, $msg;

		print $log_line;
	}

	exit 1 if $tag eq FATAL;
}

sub normalize_identifiers
{
	my ($tags) = @_;
	my %normal;

	foreach my $arr (values %$tags) {
		foreach my $ident (@$arr) {
			my $orig = $ident;
			my $lower = lc $ident;

			if (exists $normal{$lower}) {
				$ident = $normal{$lower};
			} else {
				$normal{$lower} = $ident;
			}

			logmsg(DEBUG, "Normalized '$orig' as '$ident'");
		}
	}
}

__END__

=pod

=head1 NAME

=over 4

tagger.pl - create an index and table of contents for archives

=back

=head1 SYNOPSIS

=over 4

perl B<tagger.pl> [B<options>] [--] [F<INPUT_FILE>...]

=back

=head1 DESCRIPTION

=over 4

tagger.pl creates and updates an index file and table of contents for the
specified archive files, based on a given list of tags.

=back

=head1 OPTIONS AND ARGUMENTS

=head2 Options

=over 4

=item B<--backup>

=item B<--nobackup>

Specifies whether or not a backup of the table of contents should be made
before writing out the new table of contents, as it is modified in-place. The
backup name is the name of the original table of contents with ".bak"
appended. The default is B<--backup>.

=item B<--contents>=F<FILE>, B<-c> F<FILE>

Specifies the file to which the table of contents will be written. The table
of contents will contain a list of sources and the tag group names which were
found in the sources. The format of the table of contents is comma separated
value file containing a heading line, followed by a titles line, followed by
zero or more lines containing a list of a blank value, the date of the source
(month/day/year or year/month/day format), the description of the source, the
name of the source, the document number of the source, a semicolon separated
list of tag group names found in that source, and a hyperlink to the assumed
location of the original PDF file:

  box_number,,,,,
  ,Date,Desc,Source,Number,Tags,Link
  ,month1/day1/year1,desc1,src1,num1,name1;name2;name3,=HYPERLINK("file1.pdf")
  ,month2/day2/year2,desc2,src2,num2,name2;name4;name5,=HYPERLINK("file2.pdf")
  ,month3/day3/year3,desc3,src3,num3,name1;name5,=HYPERLINK("file3.pdf")

If there are zero list lines, the titles line is optional. If no titles line
is present, the heading line in optional. The titles line is always replaced
with the built-in version:

  ,Date,Description,Source,Document #,Tags,Link

This file is created if it does not exist, with a blank heading line.
Otherwise, this file is loaded before tagging begins. The table of contents
and the index will be updated based on the loaded information, in addition to
the information gathered from tagging.

=item B<--debug>, B<-d>

Enables debugging output. This adds a significant amount of logging
information which can help with finding bugs, but it significantly reduces the
speed of processing. This is a synonym of B<--verbosity>=3.

=item B<-h>, B<-?>

Shows help information about usage, options, and arguments and exits.

=item B<--help>

Shows the full help message and all documentation information and exits.

=item B<--index>=F<FILE>, B<-i> F<FILE>

Specifies the file to which the index will be written. The index file will
contain a list of tag group names and the sources in which they were found.
The format of the index file is one or more lines containing a tag, a tab
character, and a comma separated list of each of the sources in which the tag
was found:

  Name1<TAB>source1,source2,source3
  Name2<TAB>source1,source4
  Name1<TAB>source3,source4,source5

=item B<--quiet>, B<-q>

Disables informational logging. This reduces the logging output for "quieter"
execution. Only errors will be logged. This is a synonym of B<--verbose>=1.

=item B<--usage>

Shows a short help message about the usage of this program and exits.

=item B<--verbose>=I<NUM>

Sets the verbosity to I<NUM>. The default level is 2. The following levels can
be selected:

=over 2

=item 0: Fatal errors only

=item 1: Errors and fatal errors only

=item 2: Information and all errors

=item 3: Debugging information, normal information and all errors

=back

=item B<--version>

Shows version information and exit.

=item B<--tag-list>=F<FILE>, B<-t> F<FILE>

Specifies the file containing the list of tags to search for. This option is
required. The format of the file is one of more lines containing the name to
identify the tag group by, a tab character, and one or more tags separated by
the pipe character C<|>:

  Name1<TAB>tag1|tag2|tag3
  Name2<TAB>tag3
  Name3<TAB>tag4|tag5

The name will be used to identify the tag group in the index and table
of contents outputs. The relationship between tags and names is many-to-many;
a name may have an arbitrary number of tags associated with it, and a tag may
be associated with an arbitrary number of names. Tags are not case-sensitive,
and they must contain only the letters C<a-z> and C<A-Z>, numbers C<0-9>, the
apostrophe C<'>, the full-stop C<.>, and the dash C<->. Names must not contain
quotes C<"> or commas C<,>.

=item B<->

This option enters interactive mode. The list of input files is read from
standard input. This allows the list of input files to be specified via I/O
redirection in addition to the normal file list specified on the command line.

=item B<-->

This is a special option which halts the processing of options. Anything
following it on the command line is treated as an argument.

=back

Options may be specified in any order. Long options may be specified with
C<-->. Short options may be specified with C<-> or C<-->, and may be bundled
together unless it causes ambiguity.

=head2 Arguments

=over 4

=item F<INPUT_FILE>

This specifies a file or pattern which represents one or more files to scan
for the specified tags. This may be specified more than once. File names must
follow the format S<C<B<year month day source number>>>, using two digit
years, and they must be unique with respect to that format. Note that
B<number> is not necessarily a number, and may contain the letters C<a-z> and
C<A-Z> and the numbers C<0-9>. For example:

  51 6 2 Asgard 100BC.txt
  00 12 29 Niflhel 3.txt
  03 07 7 Bifrost 9.txt

When the operating system is Windows, special "globbing" rules are applied to
the file names. In this case, the character C<*> is treated as a wild card,
matching zero or more characters. File patterns using this wild card are
internally expanded to the zero or more existing files which match the
pattern. This can be useful to specify all files in a directory, by appending
a C<*> to the directory name.

=back

=head1 EXAMPLES

=over 4

To index the file "F<51 6 2 Asgard 100BC.txt>", using F<toc.csv> as a table of
contents, F<index.txt> as an index, and F<tags.txt> for tags, run the
following:

  perl tagger.pl -t tags.txt -i index.txt -c toc.csv "51 6 2 Asgard 100BC.txt"

To index all the  F<.txt> files in the F<txt> and F<arc> folders, using
F<toc.csv> as a table of contents, F<index.txt> as an index, and F<tags.txt>
for tags, run the following:

  perl tagger.pl -t tags.txt -i index.txt -c toc.csv txt/*.txt arc/*.txt

To accomplish the same using I/O redirection, with the files to scan listed in
F<file.txt>:

  perl tagger.pl -t tags.txt -i index.txt -c toc.csv - < files.txt

Neither the index nor the table of contents is required, though not specifying
either effectively does nothing:

  perl tagger.pl -t tags.txt "51 6 2 Asgard 100BC.txt"

=back

=head1 AUTHOR

=over 4

Written by Brandon Yannoni E<lt>byannoni@gmail.comE<gt>.

=back

=cut

